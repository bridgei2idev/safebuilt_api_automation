*** Settings ***
Library     RequestsLibrary
Variables   ${EXEC_DIR}${/}Constants/common_constants.py
Resource    ${EXEC_DIR}${/}Resources/common_keywords.robot
Resource    ${EXEC_DIR}${/}Resources/user_preferences.robot
Resource    ${EXEC_DIR}${/}Resources/state_api.robot


Suite Setup     create session and token

*** Keywords ***
create session and token
    ${access_token}=   generate oauth token    ${state_session_name}
    set suite variable      ${token}        ${access_token}

*** Test Cases ***
Verify get state with valid token
     [Documentation]  Verify get state API response when token is valid
    ${resp}=    get state     ${state_session_name}      ${token}
    Validate valid state response     ${resp}


Verify get state with invalid token
     [Documentation]  Verify get state API response when token is invalid
    ${resp}=    get state    ${state_session_name}   ${invalid_token}
    verify unauthorized error   ${resp}

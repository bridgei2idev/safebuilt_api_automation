*** Settings ***
Library     RequestsLibrary
Variables   ${EXEC_DIR}${/}Constants/common_constants.py
Resource    ${EXEC_DIR}${/}Resources/common_keywords.robot
Resource    ${EXEC_DIR}${/}Resources/user_preferences_api.robot


Suite Setup     create session and token

*** Keywords ***
create session and token
    ${access_token}=   generate oauth token    safebuilt
    set suite variable      ${token}        ${access_token}

*** Test Cases ***
Verify get preferences with valid token
     [Documentation]  Verify get User Preferences API response when token is valid
    ${resp}=    get user preferences     safebuilt      ${token}
    Validate preference response     ${resp}


Verify get preferences with invalid token
     [Documentation]  Verify get User Preferences API response when token is invalid
    ${resp}=    get user preferences    safebuilt   ${invalid_token}
    verify unauthorized error   ${resp}

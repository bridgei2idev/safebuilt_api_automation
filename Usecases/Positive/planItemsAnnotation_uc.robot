*** Settings ***
Library     RequestsLibrary
Library     ${EXEC_DIR}${/}Resources/common_util.py
Variables   ${EXEC_DIR}${/}Constants/common_constants.py
Resource    ${EXEC_DIR}${/}Resources/common_keywords.robot
Resource    ${EXEC_DIR}${/}Resources/planItemsAnnotation_api.robot


Suite Setup     create session and token

*** Keywords ***
create session and token
    ${access_token}=   generate oauth token    ${add_plan_session_name}
    set suite variable      ${token}        ${access_token}

*** Test Cases ***
Verify get plan annotation
    [Documentation]  Verify get planItemsAnnotation API response when token is valid
    ${resp}=    get plan annotation     ${add_plan_session_name}   ${token}     4101    annotation
    log to console      ${resp}

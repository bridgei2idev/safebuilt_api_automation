*** Settings ***
Library     RequestsLibrary
Library     ${EXEC_DIR}${/}Resources/common_util.py
Variables   ${EXEC_DIR}${/}Constants/common_constants.py
Resource    ${EXEC_DIR}${/}Resources/common_keywords.robot
Resource    ${EXEC_DIR}${/}Resources/user_preferences.robot
Resource    ${EXEC_DIR}${/}Resources/state_api.robot
Resource    ${EXEC_DIR}${/}Resources/jurisdiction_api.robot
Resource    ${EXEC_DIR}${/}Resources/plantypes_api.robot
Resource    ${EXEC_DIR}${/}Resources/codebook_api.robot


Suite Setup     create session and token

*** Keywords ***
create session and token
    ${access_token}=   generate oauth token    ${codebook_session_name}
    set suite variable      ${token}        ${access_token}

*** Test Cases ***
Verify get codebook with valid token
     [Documentation]  Verify get codebook API response when token is valid
    ${state_id}=    get state_id        ${codebook_session_name}      ${token}      Colorado
    ${jurisdiction_id}=    get jurisdiction_id        ${codebook_session_name}      ${token}      ${state_id}   Vista
    ${today_date}=    get today date
    set suite variable      ${current_date}     ${today_date}
    log to console  Today date is
    log to console  ${today_date}
    ${resp}=    get codebook     ${codebook_session_name}      ${token}       ${state_id}   ${jurisdiction_id}  1   ${today_Date}
    Validate valid codebook response     ${resp}


Verify get preferences with invalid token
     [Documentation]  Verify get codebook API response when token is invalid
    ${resp}=    get codebook    ${codebook_session_name}   ${invalid_token}    3        23      1       ${current_date}
    verify unauthorized error   ${resp}

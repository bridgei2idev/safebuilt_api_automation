*** Settings ***
Library     RequestsLibrary
Variables   ${EXEC_DIR}${/}Constants/common_constants.py
Resource    ${EXEC_DIR}${/}Resources/common_keywords.robot
Resource    ${EXEC_DIR}${/}Resources/jurisdiction_api.robot
Resource    ${EXEC_DIR}${/}Resources/state_api.robot


Suite Setup     create session and token

*** Keywords ***
create session and token
    ${access_token}=   generate oauth token    ${jurisdiction_session_name}
    set suite variable      ${token}        ${access_token}

*** Test Cases ***
Verify get jurisdiction with valid token
     [Documentation]  Verify get jurisdiction API response when token is valid
    ${state_id}=    get state_id        ${jurisdiction_session_name}      ${token}      Colorado
    ${resp}=    get jurisdiction     ${jurisdiction_session_name}      ${token}     ${state_id}
    Validate valid jurisdiction response     ${resp}    Colorado


Verify get jurisdiction with invalid token
     [Documentation]  Verify get jurisdiction API response when token is invalid
    ${resp}=    get jurisdiction    ${jurisdiction_session_name}   ${invalid_token}     3
    verify unauthorized error   ${resp}

*** Settings ***
Library     RequestsLibrary
Variables   ${EXEC_DIR}${/}Constants/common_constants.py
Resource    ${EXEC_DIR}${/}Resources/common_keywords.robot
Resource    ${EXEC_DIR}${/}Resources/user_preferences.robot
Resource    ${EXEC_DIR}${/}Resources/state_api.robot
Resource    ${EXEC_DIR}${/}Resources/jurisdiction_api.robot
Resource    ${EXEC_DIR}${/}Resources/plantypes_api.robot


Suite Setup     create session and token

*** Keywords ***
create session and token
    ${access_token}=   generate oauth token    ${plantype_session_name}
    set suite variable      ${token}        ${access_token}

*** Test Cases ***
Verify get plantypes with valid token
     [Documentation]  Verify get plan types API response when token is valid
    ${state_id}=    get state_id        ${plantype_session_name}      ${token}      Colorado
    ${jurisdiction_id}=    get jurisdiction_id        ${plantype_session_name}      ${token}      ${state_id}   Vista
    ${resp}=    get plantypes     ${plantype_session_name}      ${token}        ${jurisdiction_id}
    Validate valid plantypes response     ${resp}


Verify get preferences with invalid token
     [Documentation]  Verify get plan types API response when token is invalid
    ${resp}=    get plantypes    ${plantype_session_name}   ${invalid_token}    23
    verify unauthorized error   ${resp}

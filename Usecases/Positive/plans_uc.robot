*** Settings ***
Library     RequestsLibrary
Library     ${EXEC_DIR}${/}Resources/common_util.py
Variables   ${EXEC_DIR}${/}Constants/common_constants.py
Resource    ${EXEC_DIR}${/}Resources/common_keywords.robot
Resource    ${EXEC_DIR}${/}Resources/user_preferences.robot
Resource    ${EXEC_DIR}${/}Resources/plan_api.robot
Resource    ${EXEC_DIR}${/}Resources/state_api.robot
Resource    ${EXEC_DIR}${/}Resources/jurisdiction_api.robot
Resource    ${EXEC_DIR}${/}Resources/codebook_api.robot


Suite Setup     create session and token

*** Keywords ***
create session and token
    ${access_token}=   generate oauth token    ${add_plan_session_name}
    set suite variable      ${token}        ${access_token}

*** Test Cases ***
Verify add plan with valid token
    [Documentation]  Verify add plan API response when token is valid
    ${state_id}=    get state_id        ${add_plan_session_name}      ${token}      Colorado
    ${jurisdiction_id}=    get jurisdiction_id        ${add_plan_session_name}      ${token}      ${state_id}   Vista
    ${today_date}=    get today date
    ${due_date}=    get due date
    ${codebook_id}=     get codebook id  ${add_plan_session_name}      ${token}      ${state_id}   ${jurisdiction_id}   1   ${today_date}
    ${plan_name}=   random test plan name
    ${plan_name}=  set variable    qa_test_plan_v3_${today_date}
    ${plantype_id}=    set variable    1
    ${job_id}   ${plan_id}=     get latest plan id and job id   ${add_plan_session_name}      ${token}
    ${resp}=    add plan     ${add_plan_session_name}      ${token}    ${plan_name}       ${jurisdiction_id}      ${codebook_id}     1,2      ${plantype_id}   ${today_date}  ${due_date}     false
    should be equal as integers     ${resp.status_code}     ${success_status_code}
    Validate add plan response     ${resp}     ${plan_name}    ${plan_id}     ${job_id}    ${plantype_id}  ${jurisdiction_id}   ${today_date}   ${due_date}
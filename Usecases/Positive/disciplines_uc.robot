*** Settings ***
Library     RequestsLibrary
Variables   ${EXEC_DIR}${/}Constants/common_constants.py
Resource    ${EXEC_DIR}${/}Resources/common_keywords.robot
Resource    ${EXEC_DIR}${/}Resources/disciplines_api.robot


Suite Setup     create session and token

*** Keywords ***
create session and token
    ${access_token}=   generate oauth token    ${discipline_session_name}
    set suite variable      ${token}        ${access_token}

*** Test Cases ***
Verify get discipline with valid token
     [Documentation]  Verify get discipline API response when token is valid
    ${resp}=    get disciplines     ${discipline_session_name}      ${token}
    Validate valid disciplines response     ${resp}


Verify get disciplines with invalid token
     [Documentation]  Verify get disciplines API response when token is invalid
    ${resp}=    get disciplines    ${discipline_session_name}   ${invalid_token}
    verify unauthorized error   ${resp}

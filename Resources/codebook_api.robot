*** Settings ***
Library     RequestsLibrary
Library     JSONLibrary
Library     Collections
Variables   ${EXEC_DIR}${/}Constants/common_constants.py
Variables   ${EXEC_DIR}${/}Constants/login_constants.py
Variables   ${EXEC_DIR}${/}Constants/endpoint_variables.py
Resource    ${EXEC_DIR}${/}Resources/common_keywords.robot



*** Keywords ***
get codebook
    [Documentation]  Keyword to call the jurisdiction API and retrieve all jurisdictions for a given state id
    [Arguments]         ${session}      ${access_token}     ${state_id}     ${jurisdiction_id}   ${plantype_id}     ${date}
    ${header}=   create dictionary     Authorization=Bearer ${access_token}
    ${params}=      create dictionary  stateid=${state_id}     jurisdictionid=${jurisdiction_id}    plantypeid=${plantype_id}   plansubmitdate=${date}
    ${response}=     get request    ${session}      ${codebook_url}      headers=${header}     params=${params}
    log to console      ${response.status_code}
    log to console      ${response.content}
    [return]    ${response}


get codebook id
    [Documentation]  Keyword to fetch the codebook id from the API response
    [Arguments]         ${session}      ${access_token}     ${state_id}     ${jurisdiction_id}   ${plantype_id}     ${date}
    ${resp}=    get codebook    ${session}      ${access_token}     ${state_id}     ${jurisdiction_id}   ${plantype_id}     ${date}
    ${data}=    set variable    ${resp.json()}
    FOR     ${code}    IN      @{data}
        ${cookbook_id}=     set variable    ${code}[id]
    END
    log to console      ${cookbook_id}
    return from keyword     ${cookbook_id}

Validate valid codebook response
    [Documentation]     Keyword to verify the response
    [Arguments]     ${resp}
    ${data}=    set variable    ${resp.json()}
    should be equal as integers  ${resp.status_code}    ${success_status_code}





*** Settings ***
Library     RequestsLibrary
Library     JSONLibrary
Library     Collections
Variables   ${EXEC_DIR}${/}Constants/common_constants.py
Variables   ${EXEC_DIR}${/}Constants/login_constants.py
Variables   ${EXEC_DIR}${/}Constants/endpoint_variables.py
Resource    ${EXEC_DIR}${/}Resources/common_keywords.robot



*** Keywords ***
get user preferences
    [Documentation]  Keyword to call the user preferences API
    [Arguments]         ${session}      ${access_token}
    ${header}=   create dictionary   Content-Type=application/json      accept=application/json     Authorization=Bearer ${access_token}
    log to console  Header value is
    log to console      ${header}
    ${response}=     get request    ${session}      ${user_preferences_url}      headers=${header}
#    should be equal as integers  ${response.status_code}    ${success_status_code}
    log to console      ${response.status_code}
    log to console      ${response.content}
    [return]    ${response}


Validate preference response
    [Documentation]     Keyword to verify the response
    [Arguments]     ${resp}
    ${data}=    set variable    ${resp.json()}
    log to console      Valid data response is
    log to console      ${data}


*** Settings ***
Library     RequestsLibrary
Library     JSONLibrary
Library     Collections
Variables   ${EXEC_DIR}${/}Constants/common_constants.py
Variables   ${EXEC_DIR}${/}Constants/login_constants.py
Variables   ${EXEC_DIR}${/}Constants/endpoint_variables.py
Resource    ${EXEC_DIR}${/}Resources/common_keywords.robot



*** Keywords ***
get state
    [Documentation]  Keyword to call the state API and retrieve all states
    [Arguments]         ${session}      ${access_token}
    ${header}=   create dictionary     Authorization=Bearer ${access_token}
    ${params}=      create dictionary  codebookenabled=true
    ${response}=     get request    ${session}      ${state_url}      headers=${header}     params=${params}
    log to console      ${response.status_code}
    log to console      ${response.content}
    [return]    ${response}

get state id value
    [Documentation]  keyowrd to get the state id from the given state details
    [Arguments]  ${state}
    ${id}=  set variable    ${state}[id]
    set suite variable      ${state_id}     ${id}

get state_id
    [Documentation]  Keyword to get the state id for given state name from the API response
    [Arguments]         ${session}      ${access_token}     ${state_name}
    ${resp}=    get state       ${session}      ${access_token}
    ${data}=    set variable    ${resp.json()}
    FOR     ${state}    IN      @{data}
        run keyword if  '${state}[stateName]' == '${state_name}'    get state id value      ${state}
    END
    log to console      ${state_id}
    return from keyword     ${state_id}

Validate valid state response
    [Documentation]     Keyword to verify the response
    [Arguments]     ${resp}
    ${data}=    set variable    ${resp.json()}
    should be equal as integers  ${resp.status_code}    ${success_status_code}
    lists should be equal   ${data}     ${states_expected_output}





*** Settings ***
Library     RequestsLibrary
Library     JSONLibrary
Library     Collections
Library     ${EXEC_DIR}${/}Resources/common_util.py
Variables   ${EXEC_DIR}${/}Constants/common_constants.py
Variables   ${EXEC_DIR}${/}Constants/endpoint_variables.py
Variables   ${EXEC_DIR}${/}TestData/common_json_requests.py
Resource    ${EXEC_DIR}${/}Encryption/decrypt_password.robot

*** Keywords ***
Create session using base url
    [Documentation]     Keyword to create session
    [Arguments]         ${session_alias}
    create session    ${session_alias}      ${base_url}
#    set global variable     ${session}      ${session_alias}

generate oauth token
    [Arguments]     ${session_name}
    Create session using base url   ${session_name}
    ${pwd}=     decrypt password using fernet   ${rev_password}
#    ${data}=      Create Dictionary     grant_type=password     client_id=safebuilt_dev    client_secret=safebuilt_dev    username=reviewer@safebuilt.com  password=admin
    ${data}=      Create Dictionary     grant_type=${grant_type}     client_id=${client_id}    client_secret=${client_secret}    username=${rev_username}  password=${pwd}

    ${headers}=   Create Dictionary      Content-Type=application/x-www-form-urlencoded
    ${resp}=      post request    ${session_name}    /oauth/token    data=${data}   headers=${headers}
    Should Be Equal As Strings  ${resp.status_code}     200
    log     ${resp}
    log to console    ${resp.content}
    ${data}=    set variable  ${resp.json()}
    log to console      ${data}
    ${access_token}=    set variable  ${data}[access_token]
    log to console      Access Token is
    log to console      ${access_token}
#    set global variable         ${token}        ${access_token}
    [return]    ${access_token}


verify unauthorized error
    [Documentation]  Keyword to verify the User unauthorized error
    [Arguments]     ${resp}
#    ${data}=    set variable  ${resp.json()}
    should be equal as integers  ${resp.status_code}    ${unauthorized_status_code}
#    log to console      ${data}


verify if integer
    [Documentation]     keyword to check if the value is an integer
    [Arguments]     ${value}
    ${boolean}=         check_if_int         ${value}
    should be true         ${boolean} is ${TRUE}


compare the dates
    [Documentation]  Keyword to compare if the given 2 dates are same
    [Arguments]     ${date1}    ${date2}
    log     ${date1}
    log     ${date2}
    should be equal as strings     ${date1}     ${date2}


random test plan name
    [Documentation]  Keyword to geneate the random plan name
    ${today_date}=  get today date
    ${random_no}=   generate random number
    return from keyword  qa_plan_v${random_no}_${today_date}
from datetime import datetime, timedelta
import difflib
import json
from random import randrange


def write_json_request(json_data, value_to_replace, final_value):
    jsonvalue = str(json_data)
    replace_string = str(final_value)
    value_to_be_replaced = str(value_to_replace)
    return jsonvalue.replace(value_to_be_replaced, replace_string)


def split_string(split_chars, value):
    if str(value).__contains__(split_chars):
        split = str(value).split(split_chars)
        return split
    else:
        print(value)
        res=[]
        res.append(value)
        return res


def split_and_remove_star_in_end(split_char,value):
    str_split=  split_string(split_char,value)
    split_after_rem_star=   list(filter(None, str_split))
    return  split_after_rem_star


def convert_str_json(json_str):
    res = json.loads(json_str)
    return res


def get_today_date():
    date = datetime.now()
    date = date.strftime("%m/%d/%Y")
    return date


def get_due_date():
    date = datetime.now()
    due_date = date+timedelta(days=15)
    due_date = due_date.strftime("%m/%d/%Y")
    return due_date


def check_if_int(value):
    res = isinstance(value, int)
    return res


def convert_milliseconds_to_date(dt):
    date_t = datetime.fromtimestamp(dt / 1000)
    date_t = date_t.strftime("%m/%d/%Y")
    return date_t


def generate_random_number():
    return randrange(1, 50)

*** Settings ***
Library     RequestsLibrary
Library     JSONLibrary
Library     Collections
Variables   ${EXEC_DIR}${/}Constants/common_constants.py
Variables   ${EXEC_DIR}${/}Constants/login_constants.py
Variables   ${EXEC_DIR}${/}Constants/endpoint_variables.py
Resource    ${EXEC_DIR}${/}Resources/common_keywords.robot



*** Keywords ***
get jurisdiction
    [Documentation]  Keyword to call the jurisdiction API and retrieve all jurisdictions for a given state id
    [Arguments]         ${session}      ${access_token}     ${state_id}
    ${header}=   create dictionary     Authorization=Bearer ${access_token}
    ${params}=      create dictionary  codebookenabled=true     jurisdictionenabled=true    codebookexists=true
    ${response}=     get request    ${session}      ${jurisdiction_url}/${state_id}      headers=${header}     params=${params}
    log to console      ${response.status_code}
    log to console      ${response.content}
    [return]    ${response}

get jurisdiction id value
    [Documentation]  keyword to get the id from the given jurisdiction data
    [Arguments]  ${jurisdiction}
    ${id}=  set variable    ${jurisdiction}[id]
    set suite variable      ${jurisdiction_id}     ${id}

get jurisdiction_id
    [Documentation]  Keyword to fetch the Jurisdiction id from the API response
    [Arguments]         ${session}      ${access_token}     ${state_id}     ${jurisdiction_name}
    ${resp}=    get jurisdiction    ${session}      ${access_token}     ${state_id}
    ${data}=    set variable    ${resp.json()}
    FOR     ${jurd}    IN      @{data}
        run keyword if  '${jurd}[jurisdictionName]' == '${jurisdiction_name}'    get jurisdiction id value      ${jurd}
    END
    log to console      ${jurisdiction_id}
    return from keyword     ${jurisdiction_id}

Validate valid jurisdiction response
    [Documentation]     Keyword to verify the response
    [Arguments]     ${resp}     ${stateName}
    ${data}=    set variable    ${resp.json()}
    should be equal as integers  ${resp.status_code}    ${success_status_code}
    lists should be equal   ${data}     ${${stateName}_jurisdictions}





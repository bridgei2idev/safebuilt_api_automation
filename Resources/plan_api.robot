*** Settings ***
Library     RequestsLibrary
Library     JSONLibrary
Library     Collections
Library     OperatingSystem
Variables   ${EXEC_DIR}${/}Constants/common_constants.py
Variables   ${EXEC_DIR}${/}Constants/login_constants.py
Variables   ${EXEC_DIR}${/}Constants/endpoint_variables.py
Resource    ${EXEC_DIR}${/}Resources/common_keywords.robot

*** Variables ***
${plan_file_path}       ${EXEC_DIR}${/}TestData/original_yellowstone_2039.pdf

*** Keywords ***
Create Multi Part
    [Documentation]  Keyword to create a payload for add plan API
    [Arguments]    ${addTo}    ${partName}     ${filePath}      ${contentType}      ${content}=${None}
    ${fileData}=    Run Keyword If    '''${content}''' != '''${None}'''     set variable    ${content}
    ...            ELSE    Get Binary File    ${filePath}
    ${fileDir}    ${fileName}=    Split Path    ${filePath}
    ${partData}=    Create List    ${fileName}    ${fileData}    ${contentType}
    Set To Dictionary    ${addTo}    ${partName}=${partData}


add plan
    [Documentation]  Keyword to add plan
    [Arguments]         ${session}      ${access_token}     ${planName}     ${jurisdictionId}   ${codebookId}   ${disciplines}  ${planTypeId}   ${submitDate}  ${dueDate}   ${sharable}
    ${header}=   create dictionary   accept=*/*     Authorization=Bearer ${access_token}
    &{fileParts}=    Create Dictionary
    Create Multi Part       ${fileParts}    file    ${plan_file_path}    application/pdf
    log to console      ${fileParts}
    ${data}=    create dictionary     planName=${planName}       jurisdictionId=${jurisdictionId}      codebookId=${codebookId}     disciplines=${disciplines}      planTypeId=${planTypeId}   planSubmitDate=${submitDate}  dueDate=${dueDate}     shareable=${sharable}
    ${response}=    post request    ${session}      ${add_plan_url}      data=${data}      headers=${header}    files=${fileParts}
    should be equal as integers  ${response.status_code}    ${success_status_code}
    log to console      ${response.status_code}
    log to console      ${response.content}
    [return]    ${response}

get latest plan details
    [Documentation]  Keyword to fetch the latest plan details
    [Arguments]     ${session}      ${access_token}
    ${header}=   create dictionary   accept=*/*     Authorization=Bearer ${access_token}
    ${params}=    create dictionary     pageNo=0       pageSize=1
    ${response}=    get request     ${session}      ${get_plans_url}      params=${params}      headers=${header}
    should be equal as integers  ${response.status_code}    ${success_status_code}
    return from keyword     ${response}


get latest plan id and job id
    [Documentation]  Keyword to fetch the latest plan and job id details using the get plan details API
    [Arguments]     ${session}      ${access_token}
    ${resp}=    get latest plan details      ${session}      ${access_token}
    ${data}=    set variable    ${resp.json()}
    ${job_id}=     set variable    ${data}[content][0][jobId]
    ${plan_id}=     set variable    ${data}[content][0][planId]
    return from keyword  ${job_id}  ${plan_id}


Validate add plan response
    [Documentation]     Keyword to verify the Add plan API response
    [Arguments]     ${resp}     ${plan_name}    ${plan_id}     ${job_id}    ${plantype_id}  ${jurisdiction_id}  ${today_date}   ${due_date}
    ${data}=    set variable    ${resp.json()}
#    ${data}=    set variable  ${resp}
    log to console      Valid data response is
    log to console      ${data}
    ${job_id}=  evaluate  ${job_id} + 1
    ${plan_id}=     evaluate    ${plan_id} + 1
    log to console      ${job_id}
    log to console      ${plan_id}
    should be equal as strings      ${data}[planName]           ${plan_name}
    should be equal as integers     ${data}[jobId]              ${job_id}
    should be equal as integers     ${data}[planId]             ${plan_id}
    should be equal as integers     ${data}[jurisdictionId]     ${jurisdiction_id}
    should be equal as strings      ${data}[planTypeId]         ${plantype_id}
    should be equal as integers     ${data}[statusId]           1
    should be equal as strings      ${data}[sourceOfPlan]       Manual
    should be equal as strings      ${data}[assignedby]         reviewer@safebuilt.com
    should be equal as strings      ${data}[reviewer]           reviewer@safebuilt.com
    should be equal                 ${data}[annotatedDate]      ${None}
    should be equal                 ${data}[verifiedDate]       ${None}
    verify_if_integer               ${data}[numberOfPages]
    ${planSetDate}=     convert milliseconds to date    ${data}[planSetDate]
    ${createdDate}=     convert milliseconds to date    ${data}[createdDate]
    ${assignedDate}=    convert milliseconds to date    ${data}[assignedDate]
    ${updatedDate}=     convert milliseconds to date    ${data}[updatedDate]
    ${dueDate}=         convert milliseconds to date    ${data}[dueDate]
    compare the dates               ${planSetDate}      ${today_date}
    compare the dates               ${createdDate}      ${today_date}
    compare the dates               ${assignedDate}     ${today_date}
    compare the dates               ${updatedDate}      ${today_date}
    compare the dates               ${dueDate}          ${due_date}

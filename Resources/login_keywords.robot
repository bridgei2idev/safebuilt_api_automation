*** Settings ***
Library     RequestsLibrary
Library     JSONLibrary
Library     Collections
Variables   ${EXEC_DIR}${/}Constants/common_constants.py
Variables   ${EXEC_DIR}${/}Constants/login_constants.py
Variables   ${EXEC_DIR}${/}Constants/endpoint_variables.py
Resource    ${EXEC_DIR}${/}Resources/common_keywords.robot



*** Keywords ***
login_user
    [Documentation]     Keyword to login
    [Arguments]      ${user_login}      ${status_code}      ${session_alias}
    Create session using base url        ${session_alias}
    ${header}=   create dictionary   Content-Type=application/json      accept=application/json
    ${response}=     post request   ${session_alias}      ${login_url}    data=${user_login}   headers=${header}
    should be equal as integers  ${response.status_code}    ${status_code}
    log  ${response.status_code}
    log  ${response.content}
    [return]    ${response}


verify_invalid_login
    [Documentation]     keyword to verify invalid login response
    [Arguments]     ${response}         ${user_login}
    ${json}=            set variable        ${response.json()}
    should be equal     ${json['msg']}             ${user_login['msg']}








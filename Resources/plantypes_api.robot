*** Settings ***
Library     RequestsLibrary
Library     JSONLibrary
Library     Collections
Variables   ${EXEC_DIR}${/}Constants/common_constants.py
Variables   ${EXEC_DIR}${/}Constants/login_constants.py
Variables   ${EXEC_DIR}${/}Constants/endpoint_variables.py
Resource    ${EXEC_DIR}${/}Resources/common_keywords.robot



*** Keywords ***
get plantypes
    [Documentation]  Keyword to call the plantypes API and retrieve all plantypes details
    [Arguments]         ${session}      ${access_token}    ${jurisdiction_id}
    ${header}=   create dictionary     Authorization=Bearer ${access_token}
    ${params}=      create dictionary  codebookenabled=true     jurisdictionenabled=true    jurisdictionid=${jurisdiction_id}
    ${response}=     get request    ${session}      ${plantypes_url}      headers=${header}     params=${params}
    log to console      ${response.status_code}
    log to console      ${response.content}
    [return]    ${response}


Validate valid plantypes response
    [Documentation]     Keyword to verify the response
    [Arguments]     ${resp}
    ${data}=    set variable    ${resp.json()}
    should be equal as integers  ${resp.status_code}    ${success_status_code}
    lists should be equal   ${data}     ${plantype_expected_output}





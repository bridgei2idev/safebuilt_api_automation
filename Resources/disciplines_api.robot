*** Settings ***
Library     RequestsLibrary
Library     JSONLibrary
Library     Collections
Variables   ${EXEC_DIR}${/}Constants/common_constants.py
Variables   ${EXEC_DIR}${/}Constants/login_constants.py
Variables   ${EXEC_DIR}${/}Constants/endpoint_variables.py
Resource    ${EXEC_DIR}${/}Resources/common_keywords.robot



*** Keywords ***
get disciplines
    [Documentation]  Keyword to call the disciplines API and retrieve all disciplines
    [Arguments]         ${session}      ${access_token}
    ${header}=   create dictionary     Authorization=Bearer ${access_token}
    ${params}=      create dictionary  codebookenabled=true
    ${response}=     get request    ${session}      ${discipline_url}      headers=${header}     params=${params}
    log to console      ${response.status_code}
    log to console      ${response.content}
    [return]    ${response}


Validate valid disciplines response
    [Documentation]     Keyword to verify the response
    [Arguments]     ${resp}
    ${data}=    set variable    ${resp.json()}
    should be equal as integers  ${resp.status_code}    ${success_status_code}
    lists should be equal   ${data}     ${disciplines_expected_output}
    ${disciplines_names}=   create list
    FOR    ${value}    IN      @{data}
       append to list  ${disciplines_names}    ${value}[name]
    END
    log to console      ${disciplines_names}
    lists should be equal   ${disciplines_names}    ${discipline_expected_names}





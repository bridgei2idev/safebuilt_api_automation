base_url="http://3.128.192.180"
success_status_code=200
error_status_code=400
unauthorized_status_code=401
network_error_msg="invalid error"
invalid_token="invalidtoken"
add_plan_session_name="add_plan_session"

state_session_name="state_session"
states_expected_output=[{"id":2,"stateName":"California"},{"id":3,"stateName":"Colorado"},{"id":8,"stateName":"Ohio"}]

discipline_session_name="discipline_session"
disciplines_expected_output=[{"id":1,"name":"Building","createdDate":1588838361642,"updatedDate":1588838361642},{"id":2,"name":"Electrical","createdDate":1588838361643,"updatedDate":1588838361643}]
discipline_expected_names=['Building','Electrical']

jurisdiction_session_name = "jurisdiction_session"
colorado_jurisdictions=[{"id":23,"jurisdictionName":"Vista"},{"id":55,"jurisdictionName":"FIRESTONE"},{"id":58,"jurisdictionName":"City and County of Denver"},{"id":59,"jurisdictionName":"City of Castle Pines"},{"id":61,"jurisdictionName":"CITY OF NORTHGLENN"},{"id":64,"jurisdictionName":"SEVERANCE"},{"id":65,"jurisdictionName":"ESTES PARK"},{"id":67,"jurisdictionName":"City of Edgewater"},{"id":68,"jurisdictionName":"City of Black Hawk"},{"id":69,"jurisdictionName":"Town of Larkspur"},{"id":70,"jurisdictionName":"Town of Pagosa Springs"},{"id":71,"jurisdictionName":"City of Longmont"},{"id":72,"jurisdictionName":"PLATTEVILLE"},{"id":73,"jurisdictionName":"Idaho Springs"},{"id":74,"jurisdictionName":"NEDERLAND"},{"id":75,"jurisdictionName":"TOWN OF MEEKER"},{"id":77,"jurisdictionName":"Town of Lochbuie"},{"id":78,"jurisdictionName":"Triangle Electric, Incorporate"},{"id":79,"jurisdictionName":"MEAD"},{"id":80,"jurisdictionName":"City of Delta"},{"id":82,"jurisdictionName":"PIERCE"},{"id":83,"jurisdictionName":"Town of Georgetown"},{"id":84,"jurisdictionName":"Town of Deer Trail"},{"id":85,"jurisdictionName":"HUDSON"},{"id":86,"jurisdictionName":"Town of Foxfield"},{"id":87,"jurisdictionName":"AULT"},{"id":88,"jurisdictionName":"GILCREST"},{"id":89,"jurisdictionName":"Town of Parachute"},{"id":91,"jurisdictionName":"Town of Minturn"},{"id":92,"jurisdictionName":"KERSEY"},{"id":93,"jurisdictionName":"Town of Limon"},{"id":96,"jurisdictionName":"Commerce City"},{"id":98,"jurisdictionName":"AMH Development"},{"id":99,"jurisdictionName":"Town of Kiowa"},{"id":100,"jurisdictionName":"PCL Construction Services, Inc"},{"id":214,"jurisdictionName":"Summit County"},{"id":254,"jurisdictionName":"Holliday ISD"},{"id":264,"jurisdictionName":"City of Meridian"},{"id":273,"jurisdictionName":"86 Newbury Street Holding LLC"}]
california_jurisdictions=[{"id":2,"jurisdictionName":"City of Encinitas"},{"id":7,"jurisdictionName":"City of Solana Beach"}]
ohio_jurisdictions=[{"id":215,"jurisdictionName":"City of Toledo"}]

plantype_session_name = "plantype_session"
plantype_expected_output=[{"id":1,"planType":"Residential"}]

codebook_session_name = "codebook_session"

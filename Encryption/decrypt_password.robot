*** Settings ***
Library     ${EXEC_DIR}${/}Encryption/decrypt_password.py

*** Variables ***
${login_pwd}=   gAAAAABgLdvpho6zpi4pTjmy1cOSkP6JioryYYx00lPUJyHUk31VWrl87RPcTBG1CYlkY86BxnG4vaR62RxjVcS5FnMuL7-ulA==

*** Keywords ***
decrypt password using fernet
    [Arguments]     ${enc_pwd}
    ${decrypted_pwd}=       decrypt_password_login        ${enc_pwd}
    [return]     ${decrypted_pwd}